from __future__ import with_statement
import io
from os import path
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from wordcloud_u import WordCloud
from wordcloud_u import STOPWORDS as stopwords_english
from wordcloud_u import ImageColorGenerator

"""
a lot of free books:
http://www.gutenberg.org

from gutenberg.acquire import load_etext
from gutenberg.cleanup import strip_headers
"""

"""
in wordcloud.py, replace:

################################################
stopwords = set(map(str.lower, self.stopwords))
################################################

with:

################################################
stopwords = []
for i in self.stopwords:
    try:
        stopwords.append(str.lower(i))
    except:
        stopwords.append(unicode.lower(i))
stopwords = set(stopwords)
################################################

for unicode support in stopwords
(August 2017)
"""

##german stopwords
#stopwords_german = set(line.strip() for line in io.open('stopwords_german.txt','r',encoding='utf8'))

##textfile from gutenberg
#text = strip_headers(load_etext(29327)).strip()

#open picture, get colors
d = path.dirname(__file__)
swift = np.array(Image.open(path.join(d, "taylor_swift.png")))
swift_colors = ImageColorGenerator(swift)

#own textfile
with io.open('swift_red.txt','r',encoding='utf8') as f:
    text = f.read()

#create WordCloud
wc = WordCloud(background_color="white", max_words=2000, stopwords=stopwords_english, mask=swift)
wc.generate(text)

# show
plt.imshow(wc.recolor(color_func=swift_colors), interpolation='bilinear')
plt.axis("off")
plt.show()
